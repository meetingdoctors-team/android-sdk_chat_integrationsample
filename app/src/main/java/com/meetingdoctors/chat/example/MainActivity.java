package com.meetingdoctors.chat.example;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.meetingdoctors.chat.MeetingDoctorsClient;
import com.meetingdoctors.chat.views.ProfessionalList;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(!MeetingDoctorsClient.getInstance().isAuthenticated()) {
            MeetingDoctorsClient.getInstance().authenticate("YOUR TOKEN", new AuthenticationListener());
        }

        ProfessionalList professionalList = (ProfessionalList)findViewById(R.id.professional_list);
        professionalList.setProfessionalListListener(new ProfessionalListListener());

        /**
         * This buttons launches medical history activity
         */
        Button medicalHistory = findViewById(R.id.medical_history);
        medicalHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MeetingDoctorsClient.getInstance().openMedicalHistory(MainActivity.this);
            }
        });
    }

    private class AuthenticationListener implements MeetingDoctorsClient.AuthenticationListener {
        @Override
        public void onAuthenticated() {
            /* your code here */
        }

        @Override
        public void onAuthenticationError() {
            /* your code here */
        }
    }

    private class ProfessionalListListener implements ProfessionalList.ProfessionalListListener {
        @Override
        public boolean onProfessionalClick(long professionalId, String speciality, boolean hasAccess, boolean isSaturated) {
            /* professionalId: professional id
               speciality: professional speciality.
               hasAccess: whether the user can chat with this professional.
                  if hasAccess is false return true would be ignored.
               isSaturated: possible delay on answers
               return value:
                  true: proceed with chat openning.
                  false: cancel chat openning. */
            return true;
        }

        @Override
        public void onListLoaded() {
            /* called after professional list if loaded */
        }

        @Override
        public void onUnreadMessageCountChange(long unreadMessageCount) {
            /* called when unread message count change */
        }
    }
}
