package com.meetingdoctors.chat.example;

import android.app.Application;
import com.meetingdoctors.chat.MeetingDoctorsClient;

public class App extends Application{
    @Override
    public void onCreate() {
        super.onCreate();

        MeetingDoctorsClient.newInstance(this, "API_KEY", 
        BuildConfig.FLAVOR.equals("debug"),
        false,
        "");
    }
}
