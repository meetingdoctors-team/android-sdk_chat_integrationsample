# MeetingDoctors SDK

# INTEGRATION STEPS #

**Table of contents**
-----------------------
- [ Changes in build.gradle ](#Changes-in-build.gradle)
- [ Initialization ](#Initialization)
- [ User authentication ](#User-authentication)
- [ ProfessionalList view ](#ProfessionalList-view)
- [ Medical history custom menu view & custom actions ](#Medical-history-custom-view-custom-actions)
- [ Firebase integration ](#Firebase-integration)
- [ Logout customer sdk ](#Logout-customer-sdk)
- [ Chat events ](#Chat-events)
- [ Customizable resources ](#Customizable-resources)
- [ Proguard resources ](#Proguard-resources)

### Changes in build.gradle ###

* Add repository.
``` gradle
repositories {
    maven {
        maven { url "https://meetingdoctors.jfrog.io/artifactory/Android/" }
    }
}
```

* Declare library in dependencies section.
``` gradle
implementation 'com.meetingdoctors:customer-chat-sdk:1.23.3'
```

In order to check the last updates please check our [ changelog ](https://bitbucket.org/meetingdoctors-team/android-sdk_chat_integrationsample/src/master/changelog.MD).

### Initialization ###

* Initialize library on your Application.onCreate() class. Api Key is a constant string provided by MeetingDoctors.
###### JAVA
``` java
MeetingDoctorsClient.Companion.newInstance(this, "<YOUR_API_KEY>",
										CustomerSdkBuildMode.DEV/STAGING/PROD,
                                        <ENABLE_DATA_ENCRYPTION> true/false,
                                        <ENCRYIPTION_PASSWORD> "yourEncryptionPassword",
                    // If ENABLE_DATA_ENCRYPTION is false -> ""
                                        <LOCALE> "yourLocaleLanguage");
                    //e.g. Locale locale = new Locale(this.getString("es");
                    //If you do not want to set specific local just set <LOCALE> to Null,
                    //Locale configuration will be handled by Android system as usual
```
###### KOTLIN
``` kotlin
MeetingDoctorsClient.newInstance(this, "<YOUR_API_KEY>",
										CustomerSdkBuildMode.DEV/STAGING/PROD,
                                        <ENABLE_DATA_ENCRYPTION> true/false,
                                        <ENCRYIPTION_PASSWORD> "yourEncryptionPassword",
                    // If ENABLE_DATA_ENCRYPTION is false -> ""
                                        <LOCALE> "yourLocaleLanguage");
                    //e.g. val locale = Locale("es")
                    //If you do not want to set specific local just set <LOCALE> to null,
                    //Locale configuration will be handled by Android system as usual
```
### User Authentication ###

* Before showing a ProfessionalList view you should authenticate user. userToken is a constant string that will allow you to validate user from your own server. Contact soporte@meetingdoctors.com for more information.
###### JAVA
``` java
        MeetingDoctorsClient.Companion.getInstance().authenticate(token,
                new MeetingDoctorsClient.AuthenticationListener() {
                    @Override
                    public void onAuthenticated() {
                        /* your code here */
                    }
                    @Override
                    public void onAuthenticationError(@NotNull Throwable throwable) {
                        /* your code here */
                    }
                });
```
###### KOTLIN
``` kotlin
MeetingDoctorsClient.instance?.authenticate(token ,object: MeetingDoctorsClient.AuthenticationListener {
                        override fun onAuthenticated() {
                            /* your code here */
                        }
                        override fun onAuthenticationError(throwable: Throwable) {
                            /* your code here */
                        }
                    })
```



### ProfessionalList View ###

* ##### Layout integration: #####
``` xml
<com.meetingdoctors.chat.views.ProfessionalList
        android:id="@+id/professional_list"
        android:layout_width="match_parent"
        android:layout_height="match_parent"/>
```
* ##### Filter professionals by speciality using white list: #####
Available specialities: generalMedicine, pediatrics, psychology, sportsMedicine, customerCare, medicalSupport, personalTraining, commercial, medicalAppointment, cardiology, gynecology, pharmacy, sexology, nutrition, fertilityConsultant.
``` xml
<com.meetingdoctors.chat.views.ProfessionalList
        android:id="@+id/professional_list"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        app:includeSpecialities="generalMedicine|pediatrics"/>
```

``` java
professionalList.includeSpecialities("generalMedicine,pediatrics");
```
* ##### Filter professionals by speciality using black list: #####
``` xml
<com.meetingdoctors.chat.views.ProfessionalList
        android:id="@+id/professional_list"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        app:excludeSpecialities="pharmacy|customerCare"/>
```
``` java
professionalList.excludeSpecialities("pharmacy,customerCare");
```
* ##### ProfessionalListListener: #####
``` java
professionalList.setProfessionalListListener(new ProfessionalListListener());
private class ProfessionalListListener implements ProfessionalList.ProfessionalListListener {
        @Override
        public boolean onProfessionalClick(long professionalId, String speciality, boolean hasAccess, boolean isSaturated) {
                /* professionalId: professional id
               speciality: professional speciality.
               hasAccess: whether the user can chat with this professional.
                  if hasAccess is false return true would be ignored.
               isSaturated: possible delay on answers
               return value:
                  true: proceed with chat openning.
                  false: cancel chat openning. */
                return true;
        }

        @Override
        public void onListLoaded() {
	            /* called after professional list if loaded */
        }

        @Override
        public void onUnreadMessageCountChange(long unreadMessageCount) {
                /* called when unread message count change */
        }
}
```
* ##### Divider View #####
A custom view can be set to divide accessible and not accessible professionals.
``` java
professionalList.setDividerView(yourView);
```
![divider](http://app.medipremium.com/divider.jpg)

### Medical History custom Menu view & custom actions ###
* (From 1.16.0 version & above)
A custom view/action can be injected to ***Medical History***'s main screen, so you can set following actions for left-top toolbar icon actions:
        - NAVIGATION_MENU_ACTION : A custom view can be injected to a NAVIGATION VIEW component.
        - BACK_NAVIGATION_ACTION : This type is quite simple, you can set a back-to-previous-screen action.

Both actions can be setted with custom icon, just create a drawable resource at your App's module with the following name: ***"home_title_bar_icon"***

* 1st step, declare which action type to implement:
###### JAVA
``` java
// here we choose to implement Navigation Drawer Menu action
MeetingDoctorsClient.Companion.getInstance().setMenuAction(MenuBaseAction.NAVIGATION_MENU_ACTION);
```

###### KOTLIN
``` KOTLIN
// here we choose to implement back-to-previous-screen action
MeetingDoctorsClient.instance?.setMenuAction(MenuBaseAction.BACK_NAVIGATION_ACTION)
```

* 2nd step, declare which action type to implement:
###### JAVA
``` java
        MeetingDoctorsClient.Companion.getInstance().saveMenuView(new YourCustomView()),
                            "Your toolbar title",
                            new MenuBaseCustomViewListener() {
                                // callback for MenuBaseAction.NAVIGATION_MENU_ACTION
                                @Override
                                public void onNavigationMenu() {}
                                // callback for MenuBaseAction.BACK_NAVIGATION_ACTION
                                @Override
                                public void onBackPressedNavigation() {}
                                // CURRENTLY NOT USED
                                @Override
                                public void onCustomAction() {}
                            })
```

###### KOTLIN
``` KOTLIN
        MeetingDoctorsClient.instance?.saveMenuView(YourCustomView(),
                "Your toolbar title",
                object : MenuBaseCustomViewListener {
                    // With kotlin you can just override your needed method depending on action type
                    // callback for MenuBaseAction.NAVIGATION_MENU_ACTION
                    override fun onNavigationMenu() {}
                    // callback for MenuBaseAction.BACK_NAVIGATION_ACTION
                    override fun onBackPressedNavigation() {}
                    // CURRENTLY NOT USED
                    override fun onCustomAction() {}
                })
```
![divider](https://drive.google.com/uc?export=view&id=1gMZm1PuUVmUqLf147lHFipgYnZpa9ieA) ![divider](https://drive.google.com/uc?export=view&id=1u8cGFuopTpDLbnxSvk-SwGIjEVeIW10C)

### Firebase Integration ###

In order to enable MeetingDoctors Chat notifications, there are two posibilities:
1.- If you have your own Firebase app declared, you must provide us Sender ID and Server Key from your Firebase workspace.
2.- If you don't have a Firebase and don't want to create it, we can provide one. For Android we need your App Package. Once Firebase app are created, we'll provide you google-services.json files to add to your apps.


### Push Notifications for Apps without their own FirebaseMessagingService ###

* Add this code inside <application> in **AndroidManifest.xml** of your project In order to allow library show push notifications. You can overwrite notification icon just defining "com.meetingdoctors.chat.notification_icon".
``` xml
<!-- meetingdoctors-lib -->
<service android:name="com.meetingdoctors.chat.fcm.MeetingDoctorsFirebaseMessagingService">
        <intent-filter>
                <action android:name="com.google.firebase.MESSAGING_EVENT"/>
        </intent-filter>
</service>
<!-- optional --><meta-data android:name="com.meetingdoctors.chat.notification_icon" android:resource="@drawable/<YOUR_DRAWABLE>" />
<!-- meetingdoctors-lib -->
```

### Push Notifications for Apps with their own FirebaseMessagingService ###
* Add the following code in your FirebaseMessagingService class:
###### JAVA
``` java
public class YourFirebaseMessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        MeetingDoctorsClient.Companion.getInstance().onFirebaseMessageReceived(remoteMessage, null);
        /* your code here */
    }
}
```

###### KOTLIN
``` KOTLIN
class YourFirebaseMessagingService: FirebaseMessagingService() {
    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)
        MeetingDoctorsClient.instance?.onFirebaseMessageReceived(p0, null)
    }
}
```

* If your App update your Firebase Token, you must update to our SDK, you can do this the following way, e.g. in your FirebaseMessagingService class:

###### JAVA
``` java
public class YourFirebaseMessagingService extends FirebaseMessagingService {
    ....
    @Override
    public void onNewToken(String token) {
        Log.i("FCM", "onNewToken() New token received on app");
        super.onNewToken(token);
        MeetingDoctorsClient.instance.onNewTokenReceived(token);
    }
}
```

###### KOTLIN
``` kotlin
class YourtFirebaseMessagingService: FirebaseMessagingService() {
    ....
    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
        MeetingDoctorsClient.instance?.onNewTokenReceived(p0)
    }
}
```


### Logout Customer SDK: ###

###### JAVA
``` java
 MeetingDoctorsClient.Companion.getInstance().deauthenticate(new OnResetDataListener() {
                    @Override
                    public void dataResetSuccessFul() {
                    	// If your App does not restart after close session you must call newInstance() method from MeetingDoctorsClient
                        //e.g. MeetingDoctorsClient.newInstance(this, "<YOUR_API_KEY>",
										true,
                                        true,
                                        "yourEncryptionPassword");

                        //DO YOUR STUFF
                        //e.g. restart your App
                    }
                    @Override
                    public void dataResetError(@Nullable Exception exception) {
                    	//DO YOUR STUFF
                        //e.g. Toast.makeText(context, "Exception: " + exception.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                    }
                });
```

###### KOTLIN
``` kotlin
MeetingDoctorsClient.instance.deauthenticate(object: OnResetDataListener {
                    override fun dataResetError(exception: Exception?) {
                    	// If your App does not restart after close session you must call newInstance() method from MeetingDoctorsClient
                        //e.g. MeetingDoctorsClient.newInstance(this, "<YOUR_API_KEY>",
										true,
                                        true,
                                        "yourEncryptionPassword");

                        //DO YOUR STUFF
                        //e.g. restart your App
                    }
                    override fun dataResetSuccessFul() {
                    	//DO YOUR STUFF
                        //e.g. Toast.makeText(context, "Exception: " + exception.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                    }
                });
```


NOTE: In order to use this feature, you should add Meetingdoctor's Security SDK:
``` gradle
    implementation 'com.meetingdoctors:meetingdoctors-security-sdk:1.0.0'
```

### Chat Events: ###
It's possible to handle several events from SDK:
- UnreadMessagesCount
- ChatView
- ProfessionalProfileView
- ChatMessageSeng
- ChatMessageReceived

**UnreadMessagesCount** can be captured calling it from Professionallist Sdk's component:
###### JAVA
``` java
professionalList.setProfessionalListListener(new ProfessionalList.ProfessionalListListener() {
            ...
            @Override
            public void onUnreadMessageCountChange(long l) {
                //Do your stuff
                // note: 'l' parameter means the total amount of user's unread messages
            }
        });
```

###### KOTLIN
``` kotlin
        professionalList.setProfessionalListListener(object: ProfessionalList.ProfessionalListListener {
        ...
            override fun onUnreadMessageCountChange(unreadMessageCount: Long) {
                //Do your stuff
                // note: 'unreadmessageCount' parameter means the total amount of user's unread messages
            }

        })
```

**Chat View, ProfesisonalProfileView, ChatMessageSent** and  **ChatMessageReceived** events are captured via **BroadcastReceiver**. Each event has its own properties, the Intent of the BroadcastReceiver contains them:

* **ChatMessageSent** & **ChatMessageReceived** Intent bundle properties:
        "eventType", "roomId", "professionalHash", "speciality", "messageType", "messageId" & "message".
    
* **ProfessionalProfileView** Intent bundle properties: 
        "eventType", "professionalHash", "speciality".
    
* **ChatView** Intent bundle properties:
        "eventType", "roomId", "professionalHash", "speciality".

###### JAVA
``` java

// 1. Create your broadcast receiver
class EventAppBroadCastReceiver extends BroadcastReceiver {
    // Capturing bundle from the intent
    Bundle eventProperties = null;

    @Override
    public void onReceive(Context context, Intent intent) {
        // Capturing bundle from the intent
        eventProperties = intent.getExtras();

        //e.g Show a toast message with info related to event 'ChatMessageReceived' by the professional
        Toast.makeText(context, "Chat event type: " + eventProperties.getString("eventType") + " " + 
                eventProperties.getString("speciality") + " " +
                eventProperties.getString("messageType") + " " +
                eventProperties.getString("message"), Toast.LENGTH_LONG).show();
    }
}

// 2. Declare your BroadcastReceiver on your Activity
BroadcastReceiver eventAppBroadCastReceiver = new EventAppBroadcastReceiver();

// 3. Register your BroadcastReceiver, e.g. on your OnResume() method
LocalBroadcastManager.getInstance(this).registerReceiver(eventAppBroadCastReceiver, new IntentFilter(getString(R.string.meetingdoctors_local_broadcast_chat_events)));

// Optional. If its needed to stop capturing events, unregister your Broadcastreceiver, e.g. on your OnPause() method
        LocalBroadcastManager.getInstance(this).unregisterReceiver(eventAppBroadCastReceiver);
```

###### KOTLIN
``` kotlin
// 1. Create your broadcast receiver
    class EventAppBroadcastReceiver : BroadcastReceiver() {
        var eventProperties: Bundle? = null
    
        override fun onReceive(context: Context?, intent: Intent) {
            // Capturing bundle from the intent
            eventProperties = intent.extras
            
            //e.g Show a toast message with info related to event 'ChatMessageReceived' by the professional
            Toast.makeText(context, "Chat event type: ${eventProperties?.getString("eventType")} " +
                    "${eventProperties?.getString("speciality")} " +
                    "${eventProperties?.getString("messageType")} " +
                    "${eventProperties?.getString("message")}", Toast.LENGTH_LONG).show()
        }
    }
    
// 2. Declare your BroadcastReceiver on your Activity
val eventAppBroadCastReceiver: BroadcastReceiver = EventAppBroadcastReceiver()

// 3. Register your BroadcastReceiver, e.g. on your OnResume() method
LocalBroadcastManager.getInstance(this).registerReceiver(eventAppBroadCastReceiver,
            IntentFilter(getString(R.string.meetingdoctors_local_broadcast_chat_events)))

// Optional. If its needed to stop capturing events, unregister your Broadcastreceiver, e.g. on your OnPause() method
        LocalBroadcastManager.getInstance(this).unregisterReceiver(eventAppBroadCastReceiver)   
```

### Customizable Resources ###
``` xml
<!-- Main color -->
<color name="meetingdoctors_base_color">@color/colorPrimary</color>
<!-- Main text color -->
<color name="meetingdoctors_text_base_color">@android:color/white</color>

<!-- ProfessionalList: Professional description text color -->
<color name="meetingdoctors_speciality_color">#000000</color>
<!-- ProfessionalList: Semi-transparent color over not accessible professionals -->
<color name="meetingdoctors_disabled_professional_color">#00000000</color>
<!-- ProfessionalList: Unread messages badge background color -->
<color name="meetingdoctors_chat_unread_messages_count_bg_color">#f12c40</color>

<!-- Chat: Date message text color -->
<color name="meetingdoctors_message_text_color_date">#4d4e52</color>
<!-- Chat: Date message background color -->
<color name="meetingdoctors_message_background_color_date">@null</color>
<!-- Chat: My text message text color -->
<color name="meetingdoctors_message_text_color_mine">#4d4e52</color>
<!-- Chat: My text message background color -->
<color name="meetingdoctors_message_background_color_mine">@null</color>
<!-- Chat: Others text message text color -->
<color name="meetingdoctors_message_text_color_their">#ffffff</color>
<!-- Chat: Others text message background color -->
<color name="meetingdoctors_message_background_color_their">@null</color>

<!-- Notification: Background color -->
<color name="meetingdoctors_notification_color">@color/mediquo_blue</color>
```

### ProGuard settings ###
* Add them in your ProGuard settings. **Only required if you're obfuscating your app, ignore otherwise**. Version 1.14.0 & above not need to add proguard rules to your project because are embedded inside SDK.

```
##---------------Begin: required by squareup libs  ----------
-keep class sun.misc.Unsafe { *; }
-dontwarn java.nio.file.*
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
-dontwarn okio.**

-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-dontwarn okhttp3.**
-dontwarn com.squareup.okhttp.**

-keepclasseswithmembers class * {
    @retrofit2.http.* <methods>;
}
-dontnote retrofit2.Platform
-dontwarn retrofit2.Platform$Java8
-keepattributes Signature
-keepattributes Exceptions

-dontwarn javax.annotation.Nullable
-dontwarn javax.annotation.ParametersAreNonnullByDefault
-dontwarn javax.annotation.concurrent.GuardedBy

-keepclassmembers class rx.internal.util.unsafe.*ArrayQueue*Field* {
     long producerIndex;
     long consumerIndex;
}
-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueProducerNodeRef {
     long producerNode;
     long consumerNode;
}
##---------------End: required by squareup libs  ----------

##---------------Begin: required by prettytime lib  ----------
-keep class org.ocpsoft.prettytime.i18n.**
##---------------End: required by prettytime lib  ----------

##---------------Begin: proguard configuration for @Keep support annotation  ----------
-keep class android.support.annotation.Keep

-keep @android.support.annotation.Keep class * {*;}

-keepclasseswithmembers class * {
    @android.support.annotation.Keep <methods>;
}

-keepclasseswithmembers class * {
    @android.support.annotation.Keep <fields>;
}

-keepclasseswithmembers class * {
    @android.support.annotation.Keep <init>(...);
}
##---------------End: proguard configuration for @Keep support annotation  ----------


##---------------Begin: proguard configuration for Glide libs  ----------
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * implements com.bumptech.glide.request.target.Target
-keep public class * extends com.bumptech.glide.module.AppGlideModule
-keep public enum com.bumptech.glide.load.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}
-dontwarn com.bumptech.glide.load.resource.bitmap.VideoDecoder
-dontwarn com.bumptech.glide.RequestBuilder
-keep public class com.bumptech.glide.RequestBuilder

##---------------End: proguard configuration for Glide libs  ----------

```